const express = require('express');


const app = express();
app.use(express.json())

// Add headers before the routes are defined
app.use(function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

//Nuevo usuario
app.post('/newuser', (req, res) => {
    
    const name = req.body.email;
    const mail = req.body.nombre_y_apellido;
    const pass = req.body.pass;
    const cPass = req.body.confirm_pass;


    let data = {
        'success': true,
        'message': `User ${req.body.nombre_y_apellido} registered correctly`,
        'data': req.body
      }
      res.json(data);
})

//Login
app.post('/login', (req, res) => {
    console.log(req.body)
    res.sendStatus(200)

})

//Servidor config
const PORT = 3000;
app.set('port', PORT);

//Levantando servidor
app.listen(PORT, () => {
    console.log(`Servidor ${app.get('port')} funcionando`)

})